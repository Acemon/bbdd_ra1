package base;

import java.io.Serializable;

/**
 * Created by Fernando on 08/11/2016.
 */
public class Raza implements Serializable{
    private String raza;
    private String Skill_1;
    private String Skill_2;
    private String Skill_3;
    private Habilidades Skill_ex;

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getSkill_1() {
        return Skill_1;
    }

    public void setSkill_1(String skill_1) {
        Skill_1 = skill_1;
    }

    public String getSkill_2() {
        return Skill_2;
    }

    public void setSkill_2(String skill_2) {
        Skill_2 = skill_2;
    }

    public String getSkill_3() {
        return Skill_3;
    }

    public void setSkill_3(String skill_3) {
        Skill_3 = skill_3;
    }

    public Habilidades getSkill_ex() {
        return Skill_ex;
    }

    public void setSkill_ex(Habilidades skill_ex) {
        Skill_ex = skill_ex;
    }

    @Override
    public String toString() {
        return raza;
    }
}
