package base;

/**
 * Created by Fernando on 08/11/2016.
 */
public enum Magias {
    Blanca,
    Negra,
    Roja,
    Azul,
    Temporal,
    Arcana,
    Sacro
}
