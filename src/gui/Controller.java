package gui;

import base.*;
import util.Constantes;
import util.Util;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by DAM on 03/11/2016.
 */
public class Controller implements ActionListener, ChangeListener, ListSelectionListener, KeyListener{
    private Model model;
    private Ventana view;
    private Integer posicion;
    private boolean nuevo;
    private String clave;
    private boolean guardado;
    private boolean campoVacio;

    /**
     * Constructor del Controller de la aplicacion.
     * @param model Acceso a @Model predefinido de @App.
     * @param view Acceso a @Ventana predefinido de @App.
     */
    public Controller(Model model, Ventana view) {
        this.model=model;
        this.view=view;
        this.nuevo=false;
        addListeners();
        addActionCommands();
        try {
            model.comprobacionFileConfig(Constantes.DEF_RUTA);
        } catch (IOException e) {
            Util.MensajeError("Error al generar el PROPS");
            e.printStackTrace();
        }
        activarInicial();
    }

    @Override
    /**
     * Realiza toda la operatoria con actionListeners como botones
     */
    public void actionPerformed(ActionEvent e) {
        Usuario usuario = null;
        Clase clase = null;
        Raza raza = null;
        String ruta = "";
        switch (e.getActionCommand()){
            case "Conectar":
                String contrasena = new String(view.tfContrasena.getPassword());

                try {
                    if (model.comprobarUserPwd(view.tfUsuario.getText(), contrasena)){
                        try {
                            model.cargarFichero();
                        } catch (IOException e1) {
                            Util.MensajeError("Error al cargar el archivo, probablemente no exista");
                        } catch (ClassNotFoundException e1) {
                            Util.MensajeError("Error al acceder");
                        }
                        refrescardlm();
                        view.btConectar.setEnabled(false);
                        view.tfUsuario.setEnabled(false);
                        view.tfContrasena.setEnabled(false);
                        view.tabbedPane1.setEnabled(true);
                        activarCampos(false);
                        view.laviso.setText("Conectado");
                        view.archivo.setEnabled(true);
                        view.xml.setEnabled(true);
                        view.sql.setEnabled(true);
                    }else{
                        Util.MensajeError("Error al acceder como usuario");
                    }
                } catch (IOException e1) {
                    Util.MensajeError("Error al acceder al PROPS");
                }
                break;
            case "Anadir":
                view.btModificar.setEnabled(false);
                view.btEliminar.setEnabled(false);
                view.tabbedPane1.setEnabled(false);
                activarCampos(true);
                limpiarTablas();
                refrescarcb();
                nuevo=true;
                view.laviso.setText("Modo Añadir");
                break;
            case "Modificar":
                view.btModificar.setEnabled(false);
                view.btEliminar.setEnabled(false);
                view.tabbedPane1.setEnabled(false);
                if (posicion==0){
                    clave=view.tfUsuNombre.getText()+" "+view.tfUsuApellido.getText();
                }else if (posicion==1){
                    clave=view.tfClaNombre.getText();
                }else{
                    clave=view.tfRazNom.getText();
                }
                activarCampos(true);
                nuevo=false;
                view.laviso.setText("Modo Modificar");
                break;
            case "Guardar":
                //Usuario
                campoVacio=false;
                if (posicion==0){
                    usuario= new Usuario();
                    usuario=rellenar(usuario);
                    if (campoVacio){
                        Util.MensajeError("Faltan datos");
                        break;
                    }else {
                        if (nuevo)
                            model.registrar(usuario);
                        if (!nuevo)
                            model.modificar(clave, usuario);
                        if (guardado){
                            try {
                                model.guardarUsuario();
                            } catch (Exception e1) {
                                Util.MensajeError("Error al guardar el usuario");
                            }
                        }
                    }

                //Clase
                }else if (posicion==1){
                    clase = new Clase();
                    clase=rellenar(clase);
                    if (campoVacio){
                        break;
                    }else{
                        if (nuevo)
                            model.registrar(clase);
                        if (!nuevo)
                            model.modificar(clave, clase);
                        if (guardado){
                            try {
                                model.guardarClase();
                            } catch (Exception e1) {
                                Util.MensajeError("Error al guardar la clase");
                            }
                        }
                    }
                //Raza
                }else{
                    raza = new Raza();
                    raza=rellenar(raza);
                    if (campoVacio){
                        Util.MensajeError("Faltan datos");
                        break;
                    }else{
                        if (nuevo)
                            model.registrar(raza);
                        if (!nuevo)
                            model.modificar(clave, raza);
                        if (guardado){
                            try {
                                model.guardarRaza();
                            } catch (Exception e1) {
                                Util.MensajeError("Error al guardar la raza");
                            }
                        }
                    }
                }
                view.tabbedPane1.setEnabled(true);
                limpiarTablas();
                limpiarcb();
                refrescardlm();
                activarCampos(false);
                view.btGuardar.setEnabled(false);
                view.btEliminar.setEnabled(false);
                view.laviso.setText("Guardado");
                break;
            case "Cancelar":
                view.tabbedPane1.setEnabled(true);
                limpiarTablas();
                limpiarcb();
                refrescardlm();
                activarCampos(false);
                view.btGuardar.setEnabled(false);
                view.btEliminar.setEnabled(false);
                view.laviso.setText("Guardado");
                break;
            case "Eliminar":
                if (posicion==0){
                    usuario = (Usuario) view.lBuscador.getSelectedValue();
                    model.eliminarUsuario(usuario.getNombre()+" "+usuario.getApellidos());
                    try {
                        model.guardarUsuario();
                    } catch (IOException e1) {
                        Util.MensajeError("Error al guardar el usuario");
                    }
                }else if (posicion==1){
                    clase = (Clase) view.lBuscador.getSelectedValue();
                    model.eliminarClase(clase.getNombre());
                    try {
                        model.guardarClase();
                    } catch (IOException e1) {
                        Util.MensajeError("Error al guardar la clase");
                    }
                }else{
                    raza = (Raza) view.lBuscador.getSelectedValue();
                    model.eliminarRaza(raza.getRaza());
                    try {
                        model.guardarRaza();
                    } catch (IOException e1) {
                        Util.MensajeError("Error al guardar la raza");
                    }
                }
                activarCampos(false);
                refrescardlm();
                view.laviso.setText("Eliminado");
                view.btGuardar.setEnabled(false);
                view.btEliminar.setEnabled(false);
                break;
            case "Guardartxt":
                ruta = cambiarRuta();
                try {
                        model.guardarUsuRuta(ruta);
                    }catch (IOException e1) {
                        Util.MensajeError("Error al guardar el usuario");
                    }
                    try {
                        model.guardarRazRuta(ruta);
                    } catch (IOException e1) {
                        Util.MensajeError("Error al guardar la raza");
                    }
                    try {
                        model.guardarClaRuta(ruta);
                    } catch (IOException e1) {
                        Util.MensajeError("Error al guardar la clase");
                    }
                    limpiarcb();
                break;
            case "Cargartxt":
                ruta = cambiarRuta();
                try {
                    model.cargarFicRuta(ruta);
                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (ClassNotFoundException e1) {
                    Util.MensajeError("No se ha podido cargar el fichero");
                }
                refrescardlm();
                break;
            case "Autoguardado":
                if (guardado){
                    guardado=false;
                    view.laviso.setText("Autoguardado off");
                    view.lguardado.setVisible(true);
                    view.btGuardaManual.setVisible(true);
                }else{
                    guardado=true;
                    view.laviso.setText("Autoguardado on");
                    view.lguardado.setVisible(false);
                    view.btGuardaManual.setVisible(false);
                }
                break;
            case "GuardaManual":
                try {
                    model.guardarUsuario();
                } catch (IOException e1) {
                    Util.MensajeError("Error al guardar el usuario");
                }
                try {
                    model.guardarClase();
                } catch (IOException e1) {
                    Util.MensajeError("Error al guardar la clase");
                }
                try {
                    model.guardarRaza();
                } catch (IOException e1) {
                    Util.MensajeError("Error al guardar la raza");
                }
                break;
            case "GuardarComo":
                ruta=cambiarRuta();
                try {
                    model.crearProps(ruta);
                } catch (IOException e1) {
                    Util.MensajeError("Imposible crear el PROPS");
                    e1.printStackTrace();
                }
                break;
            case "Importxml":
                //WIP
                break;
            case "Exportxml":
                try {
                    model.exportarXML();
                } catch (ParserConfigurationException e1) {
                    e1.printStackTrace();
                } catch (TransformerException e1) {
                    e1.printStackTrace();
                }
                break;
            case "ExportSql":
                //WIP
                break;
        }
    }

    /**
     * hace que el usuario escoja una ruta
     * @return ruta
     */
    private String cambiarRuta() {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.showOpenDialog(chooser);
        return chooser.getSelectedFile().getAbsolutePath();
    }

    @Override
    /**
     * sirve para saber en que pagina del tabbedpane estas
     */
    public void stateChanged(ChangeEvent changeEvent) {
        if (changeEvent.getSource()==view.chkMagia){
            if (view.chkMagia.isSelected()){
                view.cbClaMagia.setVisible(true);
                view.lmagia.setVisible(true);
            }else{
                view.cbClaMagia.setVisible(false);
                view.lmagia.setVisible(false);
            }
        }else{
            posicion=view.tabbedPane1.getSelectedIndex();
            if (posicion==0){
                view.lBuscador.setModel(view.dlmUsuario);
            }else if (posicion==1){
                view.lBuscador.setModel(view.dlmClase);
            }else{
                view.lBuscador.setModel(view.dlmRaza);
            }
            limpiarcb();
            view.btModificar.setEnabled(false);
            view.btEliminar.setEnabled(false);
        }

    }
    @Override
    /**
     * Permite escoger un valor de la lista
     */
    public void valueChanged(ListSelectionEvent listSelectionEvent) {
        if(listSelectionEvent.getValueIsAdjusting()){
            return;
        }
        refrescarcb();
        limpiarTablas();
        if (posicion==0){
            Usuario usuario = (Usuario) view.lBuscador.getSelectedValue();
            if (usuario==null)
                return;
            view.tfUsuNombre.setText(usuario.getNombre());
            view.tfUsuApellido.setText(usuario.getApellidos());
            view.tfUsuFecha.setDate(usuario.getFecha());
            view.cbUsuRaza.setSelectedItem(usuario.getRaza());
            view.cbUsuClase.setSelectedItem(usuario.getClase());
        }else if (posicion==1){
            Clase clase = (Clase) view.lBuscador.getSelectedValue();
            if (clase==null)
                return;
            view.tfClaNombre.setText(clase.getNombre());
            view.tfClaVida.setText(String.valueOf(clase.getSalud()));
            if (clase.getArmadura().equals(Clase.Armadura.Ligera))
                view.rbLigera.setSelected(true);
            else if (clase.getArmadura().equals(Clase.Armadura.Media))
                view.rbMedia.setSelected(true);
            else
                view.rbPesada.setSelected(true);
            if (clase.isMagia()){
                view.chkMagia.setSelected(true);
                view.cbClaMagia.setSelectedItem(clase.getTipoMagia());
            }
        }else{
            Raza raza = (Raza) view.lBuscador.getSelectedValue();
            if (raza==null)
                return;
            view.tfRazNom.setText(raza.getRaza());
            view.tfRazSkill1.setText(raza.getSkill_1());
            view.tfRazSkill2.setText(raza.getSkill_2());
            view.tfRazSkill3.setText(raza.getSkill_3());
            view.cbRazSkill.setSelectedItem(raza.getSkill_ex());
        }
        if (!view.tfUsuNombre.getText().equals("") || !view.tfClaNombre.getText().equals("") ||
                !view.tfRazNom.getText().equals("")){
            view.btModificar.setEnabled(true);
            view.btEliminar.setEnabled(true);
        }
    }

    /**
     * rellena un usuario
     * @param usuario usuario vacio
     * @return usuario rellenado
     */
    private Usuario rellenar(Usuario usuario) {
        usuario.setNombre(view.tfUsuNombre.getText());
        usuario.setApellidos(view.tfUsuApellido.getText());
        usuario.setFecha(view.tfUsuFecha.getDate());
        usuario.setRaza((Raza) view.cbUsuRaza.getSelectedItem());
        usuario.setClase((Clase) view.cbUsuClase.getSelectedItem());
        if (usuario.getNombre().equals("") || usuario.getApellidos().equals("") || usuario.getFecha().equals("")){
            this.campoVacio=true;
        }
        return usuario;
    }

    /**
     * rellena una clase
     * @param clase Clase vacia
     * @return Clase llena
     */
    private Clase rellenar(Clase clase) {
        clase.setNombre(view.tfClaNombre.getText());
        if (clase.getNombre().equals("")){
            campoVacio=true;
        }
        if (view.tfClaVida.getText()==""){
            clase.setSalud((float) 0);
        }else {
            try{
                clase.setSalud(Float.parseFloat(view.tfClaVida.getText()));
            }catch (Exception e){
                this.campoVacio=true;
                Util.MensajeError("Has introducido una cadena de texto en un campo numerico");
            }
        }
        if (view.rbLigera.isSelected()) {
            clase.setArmadura(Clase.Armadura.Ligera);
        }else if (view.rbMedia.isSelected()) {
            clase.setArmadura(Clase.Armadura.Media);
        }else if (view.rbPesada.isSelected()){
            clase.setArmadura(Clase.Armadura.Pesada);
        }else{
            Util.MensajeError("No has seleccionado una armadura");
            this.campoVacio=true;
        }
        clase.setMagia(view.chkMagia.isSelected());
        if (clase.isMagia()){
            clase.setTipoMagia((Magias) view.cbClaMagia.getSelectedItem());
        }else{
            clase.setTipoMagia(null);
        }
        return clase;
    }

    /**
     * rellena una raza
     * @param raza raza vacia
     * @return raza llena
     */
    private Raza rellenar(Raza raza) {
        raza.setRaza(view.tfRazNom.getText());
        raza.setSkill_1(view.tfRazSkill1.getText());
        raza.setSkill_2(view.tfRazSkill2.getText());
        raza.setSkill_3(view.tfRazSkill3.getText());
        raza.setSkill_ex((Habilidades) view.cbRazSkill.getSelectedItem());
        if (raza.getRaza().equals("") || raza.getSkill_1().equals("") ||
                raza.getSkill_2().equals("") || raza.getSkill_3().equals("")){
            this.campoVacio=true;
        }
        return raza;
    }

    /**
     * Pone los action command con los que yo quiero trabajar
     */
    private void addActionCommands() {
        view.btConectar.setActionCommand("Conectar");
        view.btAnadir.setActionCommand("Anadir");
        view.btModificar.setActionCommand("Modificar");
        view.btGuardar.setActionCommand("Guardar");
        view.btEliminar.setActionCommand("Eliminar");
        view.btCancelar.setActionCommand("Cancelar");
        view.btGuardaManual.setActionCommand("GuardaManual");

        view.guardar.setActionCommand("Guardartxt");
        view.cargar.setActionCommand("Cargartxt");
        view.autoguardado.setActionCommand("Autoguardado");
        view.importxml.setActionCommand("Importxml");
        view.exportxml.setActionCommand("Exportxml");
        view.exportsql.setActionCommand("ExportSql");
        view.cambiarRutaTrabajo.setActionCommand("GuardarComo");
    }

    /**
     * pone los listerner a lo que lo necesite
     */
    private void addListeners() {
        view.btConectar.addActionListener(this);
        view.btAnadir.addActionListener(this);
        view.btModificar.addActionListener(this);
        view.btGuardar.addActionListener(this);
        view.btEliminar.addActionListener(this);
        view.btCancelar.addActionListener(this);
        view.btGuardaManual.addActionListener(this);
        view.lBuscador.addListSelectionListener(this);
        view.tabbedPane1.addChangeListener(this);
        view.tfBuscar.addKeyListener(this);

        view.guardar.addActionListener(this);
        view.cargar.addActionListener(this);
        view.autoguardado.addActionListener(this);
        view.importxml.addActionListener(this);
        view.exportxml.addActionListener(this);
        view.exportsql.addActionListener(this);
        view.chkMagia.addChangeListener(this);
        view.cambiarRutaTrabajo.addActionListener(this);
    }

    /**
     * preparacion de lo que quiero que se vea al iniciar y lo que no
     */
    private void activarInicial() {
        this.clave="";
        this.posicion=0;
        this.guardado=true;
        this.campoVacio=false;
        view.tfUsuFecha.setEnabled(false);
        view.btAnadir.setEnabled(false);
        view.btModificar.setEnabled(false);
        view.lguardado.setVisible(false);
        view.btGuardaManual.setVisible(false);
        view.tabbedPane1.setEnabled(false);
        view.lBuscador.setModel(view.dlmUsuario);

        view.archivo.setEnabled(false);
        view.xml.setEnabled(false);
        view.sql.setEnabled(false);
    }

    /**
     * Activacion de campos generica
     * @param b booleano de activacion
     */
    private void activarCampos(boolean b) {
        view.tfUsuNombre.setEnabled(b);
        view.tfUsuApellido.setEnabled(b);
        view.tfUsuFecha.setEnabled(b);
        view.cbUsuRaza.setEnabled(b);
        view.cbUsuClase.setEnabled(b);

        view.tfClaNombre.setEnabled(b);
        view.tfClaVida.setEnabled(b);
        view.rbPesada.setEnabled(b);
        view.rbMedia.setEnabled(b);
        view.rbLigera.setEnabled(b);
        view.chkMagia.setEnabled(b);
        view.cbClaMagia.setEnabled(b);

        view.tfRazNom.setEnabled(b);
        view.tfRazSkill1.setEnabled(b);
        view.tfRazSkill2.setEnabled(b);
        view.tfRazSkill3.setEnabled(b);
        view.cbRazSkill.setEnabled(b);

        view.btAnadir.setEnabled(!b);
        view.tfBuscar.setEnabled(!b);
        view.lBuscador.setEnabled(!b);
        view.btGuardar.setEnabled(b);
        view.btCancelar.setEnabled(b);
    }

    /**
     * Limpia las tablas
     */
    private void limpiarTablas() {
        view.tfUsuNombre.setText("");
        view.tfUsuApellido.setText("");
        view.tfUsuFecha.setDate(null);

        view.tfClaNombre.setText("");
        view.tfClaVida.setText("");
        view.armor.clearSelection();
        view.chkMagia.setSelected(false);

        view.tfRazNom.setText("");
        view.tfRazSkill1.setText("");
        view.tfRazSkill2.setText("");
        view.tfRazSkill3.setText("");
    }

    /**
     * limpia las combo box
     */
    private void limpiarcb() {
        view.cbUsuClase.removeAllItems();
        view.cbUsuRaza.removeAllItems();
        view.cbClaMagia.removeAllItems();
        view.cbRazSkill.removeAllItems();
    }

    /**
     * rellena las combo box
     */
    private void refrescarcb() {
        limpiarcb();
        for (Clase clase : model.obtenerClase()){
            view.cbUsuClase.addItem(clase);
        }

        for (Raza raza : model.obtenerRaza()){
            view.cbUsuRaza.addItem(raza);
        }

        for (Magias magia : Magias.values()){
            view.cbClaMagia.addItem(magia);
        }

        for (Habilidades hab : Habilidades.values()){
            view.cbRazSkill.addItem(hab);
        }

    }

    /**
     * refresca las DLM
     */
    private void refrescardlm() {
        view.dlmUsuario.removeAllElements();
        for (Usuario usuario : model.obtenerUsuario()){
            view.dlmUsuario.addElement(usuario);
        }
        view.dlmClase.removeAllElements();
        for (Clase clase : model.obtenerClase()){
            view.dlmClase.addElement(clase);
        }
        view.dlmRaza.removeAllElements();
        for (Raza raza : model.obtenerRaza()){
            view.dlmRaza.addElement(raza);
        }
    }

    @Override
    /**
     * sin uso
     */
    public void keyTyped(KeyEvent e) {

    }

    @Override
    /**
     * sin uso
     */
    public void keyPressed(KeyEvent e) {

    }

    @Override
    /**
     * funcionalidad de la tfbuscar
     */
    public void keyReleased(KeyEvent e) {
        String cadena= view.tfBuscar.getText();
        if (posicion==0){
            ArrayList<Usuario>usuario;
            if (cadena.length()<3){
                usuario=new ArrayList<>(model.obtenerUsuario());
            }else{
                usuario=model.obtenerUsuario(cadena);
            }
            view.dlmUsuario.clear();
            for (Usuario user : usuario){
                view.dlmUsuario.addElement(user);
            }
        }else if (posicion==1){
            ArrayList<Clase>clase;
            if (cadena.length()<3){
                clase = new ArrayList<>(model.obtenerClase());
            }else{
                clase = model.obtenerClase(cadena);
            }
            view.dlmClase.clear();
            for (Clase clas : clase){
                view.dlmClase.addElement(clas);
            }
        }else{
            ArrayList<Raza> raza;
            if (cadena.length()<3){
                raza = new ArrayList<>(model.obtenerRaza());
            }else{
                raza = (model.obtenerRaza(cadena));
            }
            view.dlmRaza.clear();
            for (Raza raz : raza){
                view.dlmRaza.addElement(raz);
            }
        }
    }
}