package gui;

import base.Clase;
import base.Raza;
import base.Usuario;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import util.Constantes;
import util.Util;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Properties;

import static util.Constantes.DEF_RUTA;

/**
 * Created by Fernando on 08/11/2016.
 */
public class Model {
    private HashMap<String, Usuario> usuarios = new HashMap<>();
    private HashMap<String, Clase> clases = new HashMap<>();
    private HashMap<String, Raza> razas = new HashMap<>();
    private String RUTA_BASE;
    private String RUTA_CLASE;
    private String RUTA_RAZA;
    private String RUTA_USUARIO;
    private String RUTA_PROPS;
    private String RUTA_XML;
    private Properties config;

    public Model() {
        RUTA_BASE=Constantes.DEF_RUTA;
        RUTA_CLASE=RUTA_BASE+ File.separator+"Clase.dat";
        RUTA_RAZA=RUTA_BASE+ File.separator+"raza.dat";
        RUTA_USUARIO=RUTA_BASE+ File.separator+"usuario.dat";
        RUTA_PROPS=RUTA_BASE+File.separator+"Config.props";
        RUTA_XML=(System.getProperty("user.home")+File.separator+"XML.xml");
    }

    /**
     * Registra una raza en el vector
     * @param raza clase Raza
     */
    public void registrar(Raza raza) {
        razas.put(raza.getRaza(), raza);
    }

    /**
     * Rellena una clase en el vector
     * @param clase clase Clase
     */
    public void registrar(Clase clase) {
        clases.put(clase.getNombre(), clase);
    }

    /**
     * Rellena un usuario en el vector
     * @param usuario clase Usuario
     */
    public void registrar(Usuario usuario) {
        usuarios.put(usuario.getNombre()+" "+usuario.getApellidos(), usuario);
    }

    /**
     * Elimina una raza del vector
     * @param clave id clave de Raza
     */
    public void eliminarRaza(String clave) {
        razas.remove(clave);
    }

    /**
     * Elimina una clase del vector
     * @param clave id clave de Clase
     */
    public void eliminarClase(String clave) {
        clases.remove(clave);
    }

    /**
     * Elimina un usuario del vector
     * @param clave id clave de Usuario
     */
    public void eliminarUsuario(String clave) {
        usuarios.remove(clave);
    }

    /**
     * Elimina una raza y pone otra en su lugar
     * @param clave id de la raza
     * @param raza clase Raza
     */
    public void modificar(String clave, Raza raza) {
        eliminarRaza(clave);
        registrar(raza);
    }

    /**
     * Elimina una clase y pone otra
     * @param clave id de Clase
     * @param clase clase Clase
     */
    public void modificar(String clave, Clase clase) {
        eliminarClase(clave);
        registrar(clase);
    }

    /**
     * Elimina un usuario y pone otro
     * @param clave id de Usuario
     * @param usuario clase Usuario
     */
    public void modificar(String clave, Usuario usuario) {
        eliminarUsuario(clave);
        registrar(usuario);
    }

    /**
     * Devuelve todos las clases
     * @return hashmap.values
     */
    public Collection<Clase> obtenerClase() {
        return clases.values();
    }

    /**
     * Devuelve todas las razas
     * @return hashmap.values
     */
    public Collection<Raza> obtenerRaza() {
        return razas.values();
    }

    /**
     * Devuelve todos los usuarios
     * @return hashmap.values
     */
    public Collection<Usuario> obtenerUsuario(){
        return usuarios.values();
    }

    /**
     * Obtener usuario dada una cadena
     * @param cadena la susodicha cadena
     * @return un arrayList
     */
    public ArrayList<Usuario> obtenerUsuario(String cadena) {
        ArrayList<Usuario>user = new ArrayList<>();
        for (Usuario usuario : obtenerUsuario()){
            if (usuario.getNombre().contains(cadena) || usuario.getApellidos().contains(cadena)){
                user.add(usuario);
            }
        }
        return user;
    }

    /**
     * Obtener Clase dada una cadena
     * @param cadena la susodicha cadena
     * @return
     */
    public ArrayList<Clase> obtenerClase(String cadena) {
        ArrayList<Clase>clas = new ArrayList<>();
        for (Clase clase : obtenerClase()){
            if (clase.getNombre().contains(cadena)){
                clas.add(clase);
            }
        }
        return clas;
    }

    /**
     * Obtener Raza dada una cadena
     * @param cadena la susodicha cadena
     * @return
     */
    public ArrayList<Raza> obtenerRaza(String cadena) {
        ArrayList<Raza> raz = new ArrayList<>();
        for (Raza raza : obtenerRaza()){
            if (raza.getRaza().contains(cadena)){
                raz.add(raza);
            }
        }
        return raz;
    }

    /**
     * Guarda un usuario en .dat
     * @throws IOException
     */
    public void guardarUsuario() throws IOException {
        ObjectOutputStream serializador = null;
        serializador = new ObjectOutputStream(new FileOutputStream(RUTA_USUARIO));
        serializador.writeObject(usuarios);
        if (serializador!=null)
            serializador.close();
    }

    /**
     * Guarda una raza en .dat
     * @throws IOException
     */
    public void guardarRaza() throws IOException {
        ObjectOutputStream serializador = null;
        serializador = new ObjectOutputStream(new FileOutputStream(RUTA_RAZA));
        serializador.writeObject(razas);
        if (serializador!=null)
            serializador.close();
    }

    /**
     * Guarda una clase en .dat
     * @throws IOException
     */
    public void guardarClase() throws IOException {
        ObjectOutputStream serializador = null;
        serializador = new ObjectOutputStream(new FileOutputStream(RUTA_CLASE));
        serializador.writeObject(clases);
        if (serializador!=null)
            serializador.close();
    }

    /**
     * carga de fichero las 3 clases
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void cargarFichero() throws FileNotFoundException, IOException, ClassNotFoundException {
        ObjectInputStream deserializador = null;
        deserializador = new ObjectInputStream(new FileInputStream(RUTA_USUARIO));
        usuarios.clear();
        usuarios = (HashMap<String, Usuario>) deserializador.readObject();
        deserializador = new ObjectInputStream(new FileInputStream(RUTA_CLASE));
        clases.clear();
        clases = (HashMap<String, Clase>) deserializador.readObject();
        deserializador = new ObjectInputStream(new FileInputStream(RUTA_RAZA));
        razas.clear();
        razas = (HashMap<String, Raza>) deserializador.readObject();
        if (deserializador != null) {
            deserializador.close();
        }
    }

    /**
     * Guarda por Usuario ruta modificada
     * @param ruta ruta
     * @throws IOException
     */
    public void guardarUsuRuta(String ruta) throws IOException {
        ObjectOutputStream serializador = null;
        serializador = new ObjectOutputStream(new FileOutputStream(ruta+File.separator+"usuario.dat"));
        serializador.writeObject(usuarios);
        if (serializador!=null)
            serializador.close();
    }

    /**
     * Guarda por Clase ruta modificada
     * @param ruta ruta
     * @throws IOException
     */
    public void guardarClaRuta(String ruta) throws IOException {
        ObjectOutputStream serializador = null;
        serializador = new ObjectOutputStream(new FileOutputStream(ruta+File.separator+"Clase.dat"));
        serializador.writeObject(clases);
        if (serializador!=null)
            serializador.close();
    }

    /**
     * Guarda por Raza ruta modificada
     * @param ruta ruta
     * @throws IOException
     */
    public void guardarRazRuta(String ruta) throws IOException {
        ObjectOutputStream serializador = null;
        serializador = new ObjectOutputStream(new FileOutputStream(ruta+File.separator+"raza.dat"));
        serializador.writeObject(razas);
        if (serializador!=null)
            serializador.close();
    }

    /**
     * Carga por ruta modificada
     * @param ruta ruta
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void cargarFicRuta(String ruta) throws FileNotFoundException, IOException, ClassNotFoundException {
        ObjectInputStream deserializador = null;
        deserializador = new ObjectInputStream(new FileInputStream(ruta+File.separator+"usuario.dat"));
        usuarios.clear();
        usuarios = (HashMap<String, Usuario>) deserializador.readObject();
        deserializador = new ObjectInputStream(new FileInputStream(ruta+File.separator+"Clase.dat"));
        clases.clear();
        clases = (HashMap<String, Clase>) deserializador.readObject();
        deserializador = new ObjectInputStream(new FileInputStream(ruta+File.separator+"raza.dat"));
        razas.clear();
        razas = (HashMap<String, Raza>) deserializador.readObject();
        if (deserializador != null) {
            deserializador.close();
        }
    }

    /**
     * Creacion del archivo PROPS para la lectura
     * @param ruta ruta
     * @throws IOException
     */
    public void crearProps(String ruta) throws IOException {
        this.config = new Properties();
        config.setProperty("user", Constantes.DEF_USER);
        config.setProperty("password", Constantes.DEF_PWD);
        config.setProperty("ruta", ruta);
        config.store(new FileOutputStream(new File(RUTA_PROPS)),"Configuracion");
        RUTA_BASE=config.getProperty("ruta");
    }

    /**
     * Comprobacion de la existencia del archivo props
     * @param ruta ruta
     * @throws IOException
     */
    public void comprobacionFileConfig(String ruta) throws IOException {
        File file = new File(DEF_RUTA+File.separator+"Config.props");
        if(!file.exists()){
            crearProps(ruta);
        }
    }

    /**
     * Comprobacion del usuario y la cotraseña con el props
     * @param user Usuario
     * @param pwd Contraseña
     * @return
     * @throws IOException
     */
    public boolean comprobarUserPwd(String user,String pwd) throws IOException {
        this.config = new Properties();
        config.load(new FileInputStream(new File(RUTA_PROPS)));
        if (!user.equals(config.getProperty("user")) || !pwd.equals(config.getProperty("password"))) {
            return false;
        }
        return true;
    }

    /**
     * Creacion del archivo XML
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public void exportarXML() throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document documento = null;

        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        documento = dom.createDocument(null, "XML", null);

        Element raiz = documento.createElement("Rol");
        documento.getDocumentElement().appendChild(raiz);
        Element  nodoPrincipal= null, nodoDatos=null;
        Text texto = null;

        for (Usuario usuario : usuarios.values()){
            nodoPrincipal = documento.createElement("Usuario");
            raiz.appendChild(nodoPrincipal);
            nodoDatos = documento.createElement("Nombre");
            nodoPrincipal.appendChild(nodoDatos);
            texto = documento.createTextNode(usuario.getNombre());
            nodoDatos.appendChild(texto);
            nodoDatos = documento.createElement("Apellidos");
            nodoPrincipal.appendChild(nodoDatos);
            texto = documento.createTextNode(usuario.getApellidos());
            nodoDatos.appendChild(texto);
            nodoDatos = documento.createElement("Fecha");
            nodoPrincipal.appendChild(nodoDatos);
            texto = documento.createTextNode(Util.formatFecha(usuario.getFecha()));
            nodoDatos.appendChild(texto);
            // TODO: 09/11/2016
//            nodoDatos = documento.createElement("Raza");
//            nodoPrincipal.appendChild(nodoDatos);
        }

        for (Clase clase: clases.values()) {
            nodoPrincipal =  documento.createElement("Clase");
            raiz.appendChild(nodoPrincipal);
            nodoDatos = documento.createElement("Nombre");
            nodoPrincipal.appendChild(nodoDatos);
            texto = documento.createTextNode(clase.getNombre());
            nodoDatos.appendChild(texto);
            nodoDatos = documento.createElement("Salud");
            nodoPrincipal.appendChild(nodoDatos);
            texto = documento.createTextNode(String.valueOf(clase.getSalud()));
            nodoDatos.appendChild(texto);
            nodoDatos = documento.createElement("Armadura");
            nodoPrincipal.appendChild(nodoDatos);
            texto = documento.createTextNode(clase.getArmadura().toString());
            nodoDatos.appendChild(texto);
            nodoDatos = documento.createElement("Magia");
            nodoPrincipal.appendChild(nodoDatos);
            texto = documento.createTextNode(clase.getClass().toString());
            nodoDatos.appendChild(texto);
        }

        for (Raza raza: razas.values()){
            nodoPrincipal = documento.createElement("Raza");
            raiz.appendChild(nodoPrincipal);
            nodoDatos = documento.createElement("Raza");
            nodoPrincipal.appendChild(nodoDatos);
            texto = documento.createTextNode(raza.getRaza());
            nodoDatos.appendChild(texto);
            nodoDatos = documento.createElement("Skill_1");
            nodoPrincipal.appendChild(nodoDatos);
            texto = documento.createTextNode(raza.getSkill_1());
            nodoDatos.appendChild(texto);
            nodoDatos = documento.createElement("Skill_2");
            nodoPrincipal.appendChild(nodoDatos);
            texto = documento.createTextNode(raza.getSkill_2());
            nodoDatos.appendChild(texto);
            nodoDatos = documento.createElement("Skill_3");
            nodoPrincipal.appendChild(nodoDatos);
            texto = documento.createTextNode(raza.getSkill_3());
            nodoDatos.appendChild(texto);
            nodoDatos = documento.createElement("Skill_ex");
            nodoPrincipal.appendChild(nodoDatos);
            texto = documento.createTextNode(raza.getSkill_ex().toString());
            nodoDatos.appendChild(texto);
        }

        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(new File (RUTA_XML));

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);

    }
}
