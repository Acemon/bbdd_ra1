package gui;

import com.toedter.calendar.JDateChooser;

import javax.swing.*;

/**
 * Created by Fernando on 08/11/2016.
 */
public class Ventana {
    JPanel panel1;
    JButton btConectar;
    JButton btAnadir;
    JButton btModificar;
    JButton btGuardar;
    JButton btEliminar;
    JTabbedPane tabbedPane1;
    JTextField tfUsuNombre;
    JTextField tfUsuApellido;
    JTextField tfMecID;
    JPasswordField tfContrasena;
    JTextField tfUsuario;
    JComboBox cbUsuClase;
    JTextField tfBuscar;
    JList lBuscador;
    JTextField tfClaNombre;
    JTextField tfRazNom;
    JTextField tfRazSkill1;
    JTextField tfRazSkill2;
    JTextField tfRazSkill3;
    JRadioButton rbPesada;
    JRadioButton rbLigera;
    JRadioButton rbMedia;
    JComboBox cbRazSkill;
    JComboBox cbUsuRaza;
    JCheckBox chkMagia;
    JComboBox cbClaMagia;
    JDateChooser tfUsuFecha;
    JTextField tfClaVida;
    JLabel laviso;
    JLabel lmagia;
    JButton btCancelar;
    JButton btGuardaManual;
    JLabel lguardado;
    ButtonGroup armor;
    DefaultListModel dlmUsuario;
    DefaultListModel dlmClase;
    DefaultListModel dlmRaza;

    JMenuBar menuBar;
    JMenu archivo;
    JMenuItem guardar;
    JMenuItem cargar;
    JMenuItem autoguardado;
    JMenuItem cambiarRutaTrabajo;
    JMenu xml;
    JMenuItem importxml;
    JMenuItem exportxml;
    JMenu sql;
    JMenuItem exportsql;

    public Ventana(){
        JFrame frame = new JFrame("Ventana");
        frame.setJMenuBar(getMenuBar());
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(3);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        dlmUsuario = new DefaultListModel();
        dlmClase = new DefaultListModel();
        dlmRaza = new DefaultListModel();

        armor = new ButtonGroup();
        armor.add(rbLigera);
        armor.add(rbMedia);
        armor.add(rbPesada);
    }

    /**
     * Creacion del JMenuBar
     * @return JMenuBar
     */
    private JMenuBar getMenuBar() {
        menuBar = new JMenuBar();
        archivo = new JMenu("Archivo");
        guardar = new JMenuItem("Guardar como");
        cargar = new JMenuItem("Cargar desde");
        autoguardado = new JMenuItem("Autoguardado");
        cambiarRutaTrabajo = new JMenuItem("Cambiar ruta");
        xml = new JMenu("XML");
        importxml = new JMenuItem("Importar");
        exportxml = new JMenuItem("Exportar");
        sql = new JMenu("SQL");
        exportsql = new JMenuItem("Exportar");

        menuBar.add(archivo);
        archivo.add(guardar);
        archivo.add(cargar);
        archivo.add(autoguardado);
        archivo.add(cambiarRutaTrabajo);

        menuBar.add(xml);
        xml.add(importxml);
        xml.add(exportxml);

        menuBar.add(sql);
        sql.add(exportsql);
        return menuBar;
    }
}
