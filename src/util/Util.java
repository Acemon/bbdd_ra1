package util;

import javax.swing.*;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by DAM on 03/11/2016.
 */
public class Util {
    public static void MensajeError(String a){
        JOptionPane.showMessageDialog(null, a, "Error", JOptionPane.ERROR_MESSAGE);
    }
    public static String formatFecha (Date fecha){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
        return sdf.format(fecha);
    }
    public static Date parseFecha (String fecha) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
        return sdf.parse(fecha);
    }
}
